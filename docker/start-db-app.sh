#!/bin/sh

docker network create aboservice-network

docker run --name dbserver \
-e POSTGRES_USER=user \
-e POSTGRES_PASSWORD=password \
-e POSTGRES_DB=postgres \
--network aboservice-network \
-p 5432:5432 \
-d postgres

docker run --name aboservice \
-e DBSERVER=dbserver \
--network aboservice-network \
-p 8080:8080 \
-d registry.gitlab.com/seychellen/aboservice:latest