# Abo-Service

A subscription service is developed from scratch. In this context, the DDD framework Axon is tested using Event Sourcing with a PostgreSQL database as persistence (for both: Event Store and Read Model for simplicity)

The default PostgreSQL95Dialect was overwritten that you can see the payload of an event (XML) as a column (type BYTEA) in the Event Store. Per default PostgreSQL stores Blobs separately.

### Tested versions
* Java: 11
* Maven: 3.8.2
* Spring: 2.5.6
* Axon: 4.5.4
* Docker: 20.10.8

### Build
* To build the SpringBoot-JAR use `mvn clean install`. Please note, that the tests only work if a running docker container provides a PostgreSQL data base on port 5432 (s. `application.yaml`).
* To build and push a Docker container adjust the image name in the `pom.xml` and use `mvn clean install -DskipTests jib:build`.

### Start
#### PostgreSQL
To start only the database, the `/docker/start-sb.sh` script can be used.

#### Application
1. You can start the Spring application `AboverwaltungApplication` from your IDE.
2. To start the database and the application using containers (adjust the images name if necessary) use `/docker/start-db-app.sh` or `/docker/docker-compose up -d`

### Video
[Axon & Spring-Boot & Kotlin & PostgreSQL](https://www.youtube.com/watch?v=oDS0TsaDYj8&t=1149s) (Live coding in German)

