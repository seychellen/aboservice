package net.kotlincook.aboservice.ui

import net.kotlincook.aboservice.coreapi.CancelAboCommand
import net.kotlincook.aboservice.coreapi.CreateAboCommand
import net.kotlincook.aboservice.coreapi.FindAboQuery
import net.kotlincook.aboservice.coreapi.SelectRecipeCommand
import net.kotlincook.aboservice.readmodel.AboView
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.messaging.responsetypes.ResponseTypes
import org.axonframework.queryhandling.QueryGateway
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import java.util.*
import java.util.concurrent.CompletableFuture

@RequestMapping("/abos")
@RestController
class AboController(var commandGateway: CommandGateway, var queryGateway: QueryGateway) {

    data class CreateAboPayload(val eMail: String, val startDatum: LocalDate, val endeDatum: LocalDate?)

    @PostMapping("/{aboId}/create")
    fun createAbo(@PathVariable aboId: String, @RequestBody payload: CreateAboPayload): CompletableFuture<UUID> {
        return  commandGateway.send(CreateAboCommand(
            UUID.fromString(aboId), payload.eMail, payload.startDatum, payload.endeDatum))
    }


    data class SelectrecipePayload(val recipe: String)

    @PostMapping("/{aboId}/selectrecipe")
    fun selectRecipe(@PathVariable aboId: String, @RequestBody payload: SelectrecipePayload): CompletableFuture<UUID> {
        return  commandGateway.send(SelectRecipeCommand(
            UUID.fromString(aboId), payload.recipe))
    }

    data class CancelAboPayload(val endeDatum: LocalDate)

    @PostMapping("/{aboId}/cancelabo")
    fun cancelAbo(@PathVariable aboId: String, @RequestBody payload: CancelAboPayload): CompletableFuture<UUID> {
        return  commandGateway.send(CancelAboCommand(
            UUID.fromString(aboId), payload.endeDatum)
        )
    }


    @GetMapping("/{aboId}")
    fun getAbo(@PathVariable aboId: String): CompletableFuture<AboView> {
        return  queryGateway.query(FindAboQuery(UUID.fromString(aboId)),
        ResponseTypes.instanceOf(AboView::class.java))
    }

}