package net.kotlincook.aboservice.readmodel

import net.kotlincook.aboservice.coreapi.*
import org.axonframework.eventhandling.EventHandler
import org.axonframework.queryhandling.QueryHandler
import org.springframework.stereotype.Component

@Component
class AboProjector(val repository: AboViewRepository) {

    @QueryHandler
    fun handle(query: FindAboQuery): AboView? {
        return repository.findById(query.aboId).orElse(null)
    }

    @EventHandler
    fun handle(event: AboCreatedEvent) {
        val aboView = AboView(
            event.aboId, event.eMail, event.startDatum, event.endeDatum, null)
        repository.save(aboView)
    }

    @EventHandler
    fun handle(event: RecipeSelectedEvent) {
        repository.findById(event.aboId).ifPresent { aboView ->
            aboView.recipe = event.recipe
        }
    }

    @EventHandler
    fun handle(event: AboCanceledEvent) {
        repository.findById(event.aboId).ifPresent { aboView ->
            aboView.endeDatum = event.endeDatum
        }
    }

}

