package net.kotlincook.aboservice.readmodel

import org.springframework.data.jpa.repository.JpaRepository
import java.time.LocalDate
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class AboView(
    @Id val aboId: UUID,
    var eMail: String,
    var startDatum: LocalDate,
    var endeDatum: LocalDate?,
    var recipe: String?
)


interface AboViewRepository : JpaRepository<AboView, UUID>