package net.kotlincook.aboservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AboserviceApplication

fun main(args: Array<String>) {
	runApplication<AboserviceApplication>(*args)
}
