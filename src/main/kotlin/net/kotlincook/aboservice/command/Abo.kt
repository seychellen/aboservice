package net.kotlincook.aboservice.command

import net.kotlincook.aboservice.coreapi.*
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle
import org.axonframework.spring.stereotype.Aggregate
import java.time.LocalDate
import java.util.*

@Aggregate
class Abo {

    @AggregateIdentifier
    lateinit var aboId: UUID
    var eMail: String? = null
    var startDatum: LocalDate? = null
    var endeDatum: LocalDate? = null
    var recipe: String? = null


    constructor()

    @CommandHandler
    constructor(command: CreateAboCommand) {
        AggregateLifecycle.apply(AboCreatedEvent(
            command.aboId, command.eMail, command.startDatum, command.endeDatum))
    }

    @CommandHandler
    fun handle(command: SelectRecipeCommand) {
        AggregateLifecycle.apply(RecipeSelectedEvent(command.aboId, command.recipe))
    }

    @CommandHandler
    fun handle(command: CancelAboCommand) {
        AggregateLifecycle.apply(AboCanceledEvent(command.aboId, command.endeDatum  ))
    }


    @EventSourcingHandler
    fun on(event: AboCreatedEvent) {
        aboId = event.aboId
        eMail = event.eMail
        startDatum = event.startDatum
        endeDatum = event.endeDatum
    }

    @EventSourcingHandler
    fun on(event: RecipeSelectedEvent) {
        recipe = event.recipe
    }

    @EventSourcingHandler
    fun on(event: AboCanceledEvent) {
        endeDatum = event.endeDatum
    }


}

//fun main() {
//    println(UUID.randomUUID())
//}