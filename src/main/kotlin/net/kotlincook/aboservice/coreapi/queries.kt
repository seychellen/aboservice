package net.kotlincook.aboservice.coreapi

import java.util.*

data class FindAboQuery(val aboId: UUID)