package net.kotlincook.aboservice.coreapi

import org.axonframework.commandhandling.RoutingKey
import org.axonframework.modelling.command.TargetAggregateIdentifier
import java.time.LocalDate
import java.util.*

data class CreateAboCommand(
    @RoutingKey var aboId: UUID,
    var eMail: String,
    var startDatum: LocalDate,
    var endeDatum: LocalDate?
)

data class SelectRecipeCommand(
    @TargetAggregateIdentifier var aboId: UUID,
    var recipe: String
)

data class CancelAboCommand(
    @TargetAggregateIdentifier var aboId: UUID,
    var endeDatum: LocalDate
)