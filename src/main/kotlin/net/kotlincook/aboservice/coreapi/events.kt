package net.kotlincook.aboservice.coreapi

import java.time.LocalDate
import java.util.*

data class AboCreatedEvent(
    var aboId: UUID,
    var eMail: String,
    var startDatum: LocalDate,
    var endeDatum: LocalDate?
)

data class RecipeSelectedEvent(
    var aboId: UUID,
    var recipe: String
)

data class AboCanceledEvent(
    var aboId: UUID,
    var endeDatum: LocalDate
)